#include <random>
#include <celero/Celero.h>
#include <raytracer/math/matrix4.hpp>
#include <glm/mat4x4.hpp>

CELERO_MAIN

static rt::matrix4<f32> m1({
5, 6, 1, 2,  //
3, 4, 7, 8,  //
5, 1, 7, 6,  //
8, 2, 2, 4,  //
});

static rt::matrix4<f32> m2({
6, 9, 7, 2,  //
0, 1, 4, 4,  //
1, 2, 9, 6,  //
6, 5, 0, 1,  //
});

static glm::mat<4, 4, f32> m3({
5, 6, 1, 2,  //
3, 4, 7, 8,  //
5, 1, 7, 6,  //
8, 2, 2, 4,  //
});

static glm::mat<4, 4, f32> m4({
6, 9, 7, 2,  //
0, 1, 4, 4,  //
1, 2, 9, 6,  //
6, 5, 0, 1,  //
});

BASELINE(DemoSimple, Baseline, 10, 1000000)
{
  celero::DoNotOptimizeAway(1);
}

BENCHMARK(Matrix4_Mul, RT_Matrix4, 10, 710000)
{
  auto m = m1 * m2;
  celero::DoNotOptimizeAway(m[0]);
}

// BENCHMARK(Matrix4_Mul, GLM_Matrix4, 10, 710000)
//{
//  auto m = m3 * m4;
//  celero::DoNotOptimizeAway(m);
//}