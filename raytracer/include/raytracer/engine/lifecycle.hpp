#ifndef RT_ENGINE_LIFECYCLE_HPP
#define RT_ENGINE_LIFECYCLE_HPP
#pragma once

#include <raytracer/engine/typedefs.hpp>

namespace rt {

class lifecycle {
 public:
  virtual void start()
  {
  }

  virtual void update(const rt::dur_s &delta_time)
  {
  }

  virtual void render(const rt::dur_s &delta_time)
  {
  }
};

}  // namespace rt

#endif  // RT_ENGINE_LIFECYCLE_HPP