#ifndef RT_ENGINE_LOG_HPP
#define RT_ENGINE_LOG_HPP
#pragma once

#include <raytracer/engine/typedefs.hpp>

namespace rt {

class log {
 private:
  log();

 public:
  static void init();

  static const char *pattern;
  static std::shared_ptr<spdlog::logger> console;
};

}  // namespace rt

#endif  // RT_ENGINE_LOG_HPP
