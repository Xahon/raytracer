#ifndef RT_ENGINE_RAYTRACER_HPP
#define RT_ENGINE_RAYTRACER_HPP
#pragma once

#include <raytracer/engine/typedefs.hpp>
#include <raytracer/utils/timer.hpp>
#include <raytracer/objects/scene.hpp>
#include <raytracer/objects/camera.hpp>
#include <raytracer/engine/lifecycle.hpp>

namespace rt {

class raytracer : public rt::lifecycle {
 private:
  SDL_Window *w = nullptr;
  SDL_GLContext gl = nullptr;

  bool is_running = false;
  bool is_stopped = false;

  rt::timer start_timer;
  rt::dur_s last_frame_duration{};
  rt::dur_s frame_duration{};
  rt::dur_s delta_duration{};

  rt::scene *active_scene = nullptr;
  rt::camera *active_camera = nullptr;

 public:
  int max_fps = 60;
  std::string w_name = "Raytracer";
  int w_width = 640;
  int w_height = 480;

  raytracer();
  virtual ~raytracer();

  void init();
  void run(void (*before_update_func)(const rt::dur_s &) = nullptr);
  void set_active_scene(rt::scene *scene, rt::camera *camera);
  rt::dur_s &time_per_frame();

 private:
  void start() override;
  void update(const rt::dur_s &delta_time) override;
  void render(const rt::dur_s &delta_time) override;
  void handle_events();
};

}  // namespace rt

#endif  // RT_ENGINE_RAYTRACER_HPP
