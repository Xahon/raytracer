#ifndef RT_ENGINE_TYPEDEFS_HPP
#define RT_ENGINE_TYPEDEFS_HPP
#pragma once

#define GL_GLEXT_PROTOTYPES

#include <array>
#include <chrono>
#include <cstring>
#include <functional>
#include <fstream>
#include <optional>
#include <iostream>
#include <map>
#include <memory>
#include <mutex>
#include <set>
#include <sstream>
#include <string>
#include <type_traits>
#include <vector>
#include <utility>
#include <spdlog/spdlog.h>
#include <spdlog/sinks/stdout_color_sinks.h>
#include <spdlog/sinks/rotating_file_sink.h>
#include <glm/glm.hpp>
#include <glm/gtx/quaternion.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/matrix_access.hpp>
#include <GL/glew.h>
#include <GL/gl.h>
#include <GL/glu.h>
#include <SDL2/SDL.h>
#include <SDL2/SDL_opengl.h>

namespace rt {

typedef GLfloat fl_t;
typedef GLuint idx_t;

// todo: add dependent conditions
typedef unsigned short u16;
typedef unsigned int u32;
typedef unsigned long u64;
typedef float f32;
typedef double f64;
typedef long double f96;

template <typename T>
using dur = std::chrono::duration<f32, T>;

using dur_ms = rt::dur<std::milli>;
using dur_s = rt::dur<std::ratio<1>>;

}  // namespace rt

#define REQUIRES(...) typename std::enable_if_t<(__VA_ARGS__)>
#define BASEOF(B, D) std::is_base_of<B, D>::value

#endif  // RT_ENGINE_TYPEDEFS_HPP