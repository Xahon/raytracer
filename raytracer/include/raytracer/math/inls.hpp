#ifndef RT_MATH_INLS_HPP
#define RT_MATH_INLS_HPP
#pragma once

#include "matrix4.hpp"
#include "vector3.hpp"
#include "quaternion.hpp"

#include "matrix4.inl"
#include "vector3.inl"
#include "quaternion.inl"

#endif  // RT_MATH_INLS_HPP