#ifndef RT_MATH_MATH_HPP
#define RT_MATH_MATH_HPP
#pragma once

namespace rt {
template <typename T>
class vector3;

template <typename T>
class matrix4;

class quaternion;
}  // namespace rt

#include <raytracer/math/math_ops.hpp>
#include <raytracer/math/vector3.hpp>
#include <raytracer/math/matrix4.hpp>

#endif  // RT_MATH_MATH_HPP
