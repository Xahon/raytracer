#ifndef RT_MATH_MATH_OPS_HPP
#define RT_MATH_MATH_OPS_HPP
#pragma once

#include <cmath>
#include <raytracer/engine/typedefs.hpp>

namespace rt {

#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Wunused-const-variable"
constexpr static const f64 floating_prec_low = 1e-4;
constexpr static const f64 floating_prec_medium = 1e-6;
constexpr static const f64 floating_prec_high = 1e-8;
constexpr static const f64 floating_prec_higher = 1e-10;
constexpr static const f64 PI = 3.14159265359;
constexpr static const f64 PI_2 = 3.14159265359 * 2;
constexpr static const f64 PI_4 = 3.14159265359 * 4;
constexpr static const f64 PI_half = 3.14159265359 / 2;
constexpr static const f64 PI_quart = 3.14159265359 / 4;
#pragma clang diagnostic pop

template <typename T>
bool eq(T e1, T e2, T prec = T(floating_prec_high))
{
  return (e1 - e2) <= prec;
}

bool eq(const f32 e1, const f32 e2, const f32 prec = floating_prec_medium)
{
  return (e1 - e2) <= prec;
}

bool eq(const f64 e1, const f64 e2, const f64 prec = floating_prec_high)
{
  return (e1 - e2) <= prec;
}

template <typename T>
bool is_0(T e1, T prec = T(floating_prec_high))
{
  return e1 <= prec;
}

bool is_0(const f32 e1, const f32 prec = floating_prec_medium)
{
  return e1 <= prec;
}

bool is_0(const f64 e1, const f64 prec = floating_prec_high)
{
  return e1 <= prec;
}

template <typename T>
bool gt(T e1, T e2, T prec = T(floating_prec_high))
{
  return (e1 - e2) > prec;
}

bool gt(const f32 e1, const f32 e2, const f32 prec = floating_prec_medium)
{
  return (e1 - e2) > prec;
}

bool gt(const f64 e1, const f64 e2, const f64 prec = floating_prec_high)
{
  return (e1 - e2) > prec;
}

template <typename T>
bool gte(T e1, T e2, T prec = T(floating_prec_high))
{
  return gt(e1, e2, prec) || eq(e1, e2, prec);
}

bool gte(const f32 e1, const f32 e2, const f32 prec = floating_prec_medium)
{
  return gt(e1, e2, prec) || eq(e1, e2, prec);
}

bool gte(const f64 e1, const f64 e2, const f64 prec = floating_prec_high)
{
  return gt(e1, e2, prec) || eq(e1, e2, prec);
}

template <typename T>
bool lt(T e1, T e2, T prec = T(floating_prec_high))
{
  return (e2 - e1) > prec;
}

bool lt(const f32 e1, const f32 e2, const f32 prec = floating_prec_medium)
{
  return (e2 - e1) > prec;
}

bool lt(const f64 e1, const f64 e2, const f64 prec = floating_prec_high)
{
  return (e2 - e1) > prec;
}

template <typename T>
bool lte(T e1, T e2, T prec = T(floating_prec_high))
{
  return lt(e1, e2, prec) || eq(e1, e2, prec);
}

bool lte(const f32 e1, const f32 e2, const f32 prec = floating_prec_medium)
{
  return lt(e1, e2, prec) || eq(e1, e2, prec);
}

bool lte(const f64 e1, const f64 e2, const f64 prec = floating_prec_high)
{
  return lt(e1, e2, prec) || eq(e1, e2, prec);
}

template <typename T>
f64 recp(T e1)
{
  return 1.0 / e1;
}

template <typename T>
f64 sqrt(T e1)
{
  return std::sqrt(e1);
}

template <typename T>
f64 recp_sqrt(T e1)
{
  return 1.0 / std::sqrt(e1);
}

}  // namespace rt

#endif  // RT_MATH_MATH_OPS_HPP
