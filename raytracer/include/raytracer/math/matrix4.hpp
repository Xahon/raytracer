#ifndef RT_MATH_MATRIX_4_HPP
#define RT_MATH_MATRIX_4_HPP
#pragma once

#include <raytracer/math/math_ops.hpp>

namespace rt {

enum matrix4_single_init_style_op {
  RT_MAT_INIT_DIAGONAL,  //
  RT_MAT_INIT_FILL_ALL,  //
};

enum matrix4_init_style_op {
  RT_MAT_INIT_NOTHING,             //
  RT_MAT_INIT_INVERTED,            //
  RT_MAT_INIT_TRANSPOSED,          //
  RT_MAT_INIT_INVERSE_TRANSPOSED,  //
};

template <typename T = f32>
class matrix4 {
 private:
  std::array<T, 16> V;

 public:
  matrix4();
  explicit matrix4(T single, matrix4_single_init_style_op op = RT_MAT_INIT_DIAGONAL);
  explicit matrix4(const std::array<T, 16> &array, matrix4_init_style_op op = RT_MAT_INIT_NOTHING);
  matrix4(std::initializer_list<T> il, matrix4_init_style_op op = RT_MAT_INIT_NOTHING);
  matrix4(const matrix4 &other, matrix4_init_style_op op = RT_MAT_INIT_NOTHING);
  matrix4(matrix4 &&other, matrix4_init_style_op op = RT_MAT_INIT_NOTHING) noexcept;
  ~matrix4();

  const T *begin() const;
  T *begin();
  const T *end() const;
  T *end();

  matrix4 &operator=(const matrix4 &other);
  matrix4 &operator=(matrix4 &&other) noexcept;
  matrix4 &operator=(std::initializer_list<T> il);
  matrix4 &operator=(const std::array<T, 16> &array);

  bool operator==(const matrix4 &rhs) const;
  bool operator!=(const matrix4 &rhs) const;

  T &operator[](size_t index);
  const T &operator[](size_t index) const;

  T &operator()(size_t row, size_t col);
  const T &operator()(size_t row, size_t col) const;

  matrix4 operator+(const matrix4 &rhs) const;
  matrix4 &operator+=(const matrix4 &rhs);
  template <typename Scalar>
  matrix4 operator+(Scalar scalar) const;
  template <typename Scalar>
  matrix4 &operator+=(Scalar scalar);

  matrix4 operator-(const matrix4 &rhs) const;
  matrix4 &operator-=(const matrix4 &rhs);
  template <typename Scalar>
  matrix4 operator-(Scalar scalar) const;
  template <typename Scalar>
  matrix4 &operator-=(Scalar scalar);

  matrix4 operator*(const matrix4 &rhs) const;
  matrix4 &operator*=(const matrix4 &rhs);
  template <typename Scalar>
  matrix4 operator*(Scalar scalar) const;
  template <typename Scalar>
  matrix4 &operator*=(Scalar scalar);

  [[nodiscard]] f64 determinant() const;

  [[nodiscard]] bool inverted(matrix4 &inverse) const;
  [[nodiscard]] bool invert();

  [[nodiscard]] matrix4 transposed() const;
  matrix4 &transpose();
  [[nodiscard]] bool is_identity() const;

  // Cross Ops

  matrix4 &set_position(const vector3<T> &vec);
  matrix4 &translate(const vector3<T> &vec);

  //

 private:
  void set_all(T value);
  void fill_data(const T *ptr);
  void fill_with_il(std::initializer_list<T> il);
  void handle_init_style_op(matrix4_init_style_op op);
};

}  // namespace rt

#include "inls.hpp"
#endif  // RT_MATH_MATRIX_4_HPP