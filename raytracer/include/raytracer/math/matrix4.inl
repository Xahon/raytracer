#ifndef RT_MATH_MATRIX_4_INL
#define RT_MATH_MATRIX_4_INL
#pragma once

#include "matrix4.hpp"

using namespace rt;

template <typename T>
matrix4<T>::matrix4()
{
  set_all(T(0));
}

template <typename T>
matrix4<T>::matrix4(T single, matrix4_single_init_style_op op)
{
  switch (op) {
    case matrix4_single_init_style_op::RT_MAT_INIT_DIAGONAL:
      set_all(T(0));
      V[0] = V[5] = V[10] = V[15] = single;
      break;
    case matrix4_single_init_style_op::RT_MAT_INIT_FILL_ALL:
      set_all(single);
      break;
  }
}

template <typename T>
matrix4<T>::matrix4(const std::array<T, 16> &array, matrix4_init_style_op op)
{
  fill_data(array.begin());
  handle_init_style_op(op);
}

template <typename T>
matrix4<T>::matrix4(std::initializer_list<T> il, matrix4_init_style_op op)
{
  fill_with_il(il);
  handle_init_style_op(op);
}

template <typename T>
matrix4<T>::matrix4(const matrix4 &other, matrix4_init_style_op op)
{
  fill_data(other.begin());
  handle_init_style_op(op);
}

template <typename T>
matrix4<T>::matrix4(matrix4 &&other, matrix4_init_style_op op) noexcept
{
  std::move(other.begin(), other.end(), begin());
  handle_init_style_op(op);
}

template <typename T>
matrix4<T>::~matrix4() = default;

template <typename T>
const T *matrix4<T>::begin() const
{
  return V.begin();
}

template <typename T>
T *matrix4<T>::begin()
{
  return V.begin();
}

template <typename T>
const T *matrix4<T>::end() const
{
  return V.end();
}

template <typename T>
T *matrix4<T>::end()
{
  return V.end();
}

template <typename T>
matrix4<T> &matrix4<T>::operator=(const std::array<T, 16> &array)
{
  fill_data(array.begin());
  return *this;
}

template <typename T>
matrix4<T> &matrix4<T>::operator=(matrix4<T> &&other) noexcept
{
  std::move(other.begin(), other.end(), begin());
  return *this;
}

template <typename T>
matrix4<T> &matrix4<T>::operator=(std::initializer_list<T> il)
{
  fill_with_il(il);
  return *this;
}

template <typename T>
matrix4<T> &matrix4<T>::operator=(const matrix4<T> &other)
{
  std::copy(other.begin(), other.end(), begin());
  return *this;
}

template <typename T>
T &matrix4<T>::operator[](size_t index)
{
  return V[index];
}

template <typename T>
const T &matrix4<T>::operator[](size_t index) const
{
  return V[index];
}

template <typename T>
T &matrix4<T>::operator()(size_t row, size_t col)
{
  return V[row * 4 + col];
}

template <typename T>
const T &matrix4<T>::operator()(size_t row, size_t col) const
{
  return V[row * 4 + col];
}

template <typename T>
bool matrix4<T>::operator==(const matrix4<T> &rhs) const
{
  if (this == &rhs)
    return true;
  return std::equal(begin(), end(), rhs.begin());
}

template <typename T>
bool matrix4<T>::operator!=(const matrix4<T> &rhs) const
{
  return !(*this == rhs);
}

template <typename T>
matrix4<T> matrix4<T>::operator+(const matrix4<T> &rhs) const
{
  matrix4 t(*this);
  t += rhs;
  return t;
}

template <typename T>
matrix4<T> &matrix4<T>::operator+=(const matrix4<T> &rhs)
{
  V[0] += rhs.V[0];
  V[1] += rhs.V[1];
  V[2] += rhs.V[2];
  V[3] += rhs.V[3];
  V[4] += rhs.V[4];
  V[5] += rhs.V[5];
  V[6] += rhs.V[6];
  V[7] += rhs.V[7];
  V[8] += rhs.V[8];
  V[9] += rhs.V[9];
  V[10] += rhs.V[10];
  V[11] += rhs.V[11];
  V[12] += rhs.V[12];
  V[13] += rhs.V[13];
  V[14] += rhs.V[14];
  V[15] += rhs.V[15];
  return *this;
}

template <typename T>
template <typename Scalar>
matrix4<T> matrix4<T>::operator+(Scalar scalar) const
{
  matrix4 t(*this);
  t += T(scalar);
  return t;
}

template <typename T>
template <typename Scalar>
matrix4<T> &matrix4<T>::operator+=(Scalar scalar)
{
  auto sc = T(scalar);
  V[0] += sc;
  V[1] += sc;
  V[2] += sc;
  V[3] += sc;
  V[4] += sc;
  V[5] += sc;
  V[6] += sc;
  V[7] += sc;
  V[8] += sc;
  V[9] += sc;
  V[10] += sc;
  V[11] += sc;
  V[12] += sc;
  V[13] += sc;
  V[14] += sc;
  V[15] += sc;
  return *this;
}

template <typename T>
matrix4<T> matrix4<T>::operator-(const matrix4<T> &rhs) const
{
  matrix4 t(*this);
  t -= rhs;
  return t;
}

template <typename T>
matrix4<T> &matrix4<T>::operator-=(const matrix4<T> &rhs)
{
  V[0] -= rhs.V[0];
  V[1] -= rhs.V[1];
  V[2] -= rhs.V[2];
  V[3] -= rhs.V[3];
  V[4] -= rhs.V[4];
  V[5] -= rhs.V[5];
  V[6] -= rhs.V[6];
  V[7] -= rhs.V[7];
  V[8] -= rhs.V[8];
  V[9] -= rhs.V[9];
  V[10] -= rhs.V[10];
  V[11] -= rhs.V[11];
  V[12] -= rhs.V[12];
  V[13] -= rhs.V[13];
  V[14] -= rhs.V[14];
  V[15] -= rhs.V[15];
  return *this;
}

template <typename T>
template <typename Scalar>
matrix4<T> matrix4<T>::operator-(Scalar scalar) const
{
  matrix4 t(*this);
  t -= T(scalar);
  return t;
}

template <typename T>
template <typename Scalar>
matrix4<T> &matrix4<T>::operator-=(Scalar scalar)
{
  auto sc = T(scalar);
  V[0] -= sc;
  V[1] -= sc;
  V[2] -= sc;
  V[3] -= sc;
  V[4] -= sc;
  V[5] -= sc;
  V[6] -= sc;
  V[7] -= sc;
  V[8] -= sc;
  V[9] -= sc;
  V[10] -= sc;
  V[11] -= sc;
  V[12] -= sc;
  V[13] -= sc;
  V[14] -= sc;
  V[15] -= sc;
  return *this;
}

template <typename T>
matrix4<T> matrix4<T>::operator*(const matrix4<T> &rhs) const
{
  matrix4 t(*this);
  t *= rhs;
  return t;
}

template <typename T>
matrix4<T> &matrix4<T>::operator*=(const matrix4<T> &rhs)
{
  /*
                  | 0  1  2  3  |
                  | 4  5  6  7  |
                  | 8  9  10 11 |
                  | 12 13 14 15 |
    | 0  1  2  3  |
    | 4  5  6  7  |
    | 8  9  10 11 |
    | 12 13 14 15 |

  */

  const matrix4 tmp(*this);

  V[0] = tmp[0] * rhs[0] + tmp[1] * rhs[4] + tmp[2] * rhs[8] + tmp[3] * rhs[12];
  V[1] = tmp[0] * rhs[1] + tmp[1] * rhs[5] + tmp[2] * rhs[9] + tmp[3] * rhs[13];
  V[2] = tmp[0] * rhs[2] + tmp[1] * rhs[6] + tmp[2] * rhs[10] + tmp[3] * rhs[14];
  V[3] = tmp[0] * rhs[3] + tmp[1] * rhs[7] + tmp[2] * rhs[11] + tmp[3] * rhs[15];

  V[4] = tmp[4] * rhs[0] + tmp[5] * rhs[4] + tmp[6] * rhs[8] + tmp[7] * rhs[12];
  V[5] = tmp[4] * rhs[1] + tmp[5] * rhs[5] + tmp[6] * rhs[9] + tmp[7] * rhs[13];
  V[6] = tmp[4] * rhs[2] + tmp[5] * rhs[6] + tmp[6] * rhs[10] + tmp[7] * rhs[14];
  V[7] = tmp[4] * rhs[3] + tmp[5] * rhs[7] + tmp[6] * rhs[11] + tmp[7] * rhs[15];

  V[8] = tmp[8] * rhs[0] + tmp[9] * rhs[4] + tmp[10] * rhs[8] + tmp[11] * rhs[12];
  V[9] = tmp[8] * rhs[1] + tmp[9] * rhs[5] + tmp[10] * rhs[9] + tmp[11] * rhs[13];
  V[10] = tmp[8] * rhs[2] + tmp[9] * rhs[6] + tmp[10] * rhs[10] + tmp[11] * rhs[14];
  V[11] = tmp[8] * rhs[3] + tmp[9] * rhs[7] + tmp[10] * rhs[11] + tmp[11] * rhs[15];

  V[12] = tmp[12] * rhs[0] + tmp[13] * rhs[4] + tmp[14] * rhs[8] + tmp[15] * rhs[12];
  V[13] = tmp[12] * rhs[1] + tmp[13] * rhs[5] + tmp[14] * rhs[9] + tmp[15] * rhs[13];
  V[14] = tmp[12] * rhs[2] + tmp[13] * rhs[6] + tmp[14] * rhs[10] + tmp[15] * rhs[14];
  V[15] = tmp[12] * rhs[3] + tmp[13] * rhs[7] + tmp[14] * rhs[11] + tmp[15] * rhs[15];

  return *this;
}

template <typename T>
template <typename Scalar>
matrix4<T> matrix4<T>::operator*(Scalar scalar) const
{
  matrix4 t(*this);
  t *= T(scalar);
  return t;
}

template <typename T>
template <typename Scalar>
matrix4<T> &matrix4<T>::operator*=(Scalar scalar)
{
  auto sc = T(scalar);
  V[0] *= sc;
  V[1] *= sc;
  V[2] *= sc;
  V[3] *= sc;
  V[4] *= sc;
  V[5] *= sc;
  V[6] *= sc;
  V[7] *= sc;
  V[8] *= sc;
  V[9] *= sc;
  V[10] *= sc;
  V[11] *= sc;
  V[12] *= sc;
  V[13] *= sc;
  V[14] *= sc;
  V[15] *= sc;
  return *this;
}

template <typename T>
f64 matrix4<T>::determinant() const
{
  if (is_identity())
    return 1;

  /*
    | 0  1  2  3  |
    | 4  5  6  7  |
    | 8  9  10 11 |
    | 12 13 14 15 |

    0 * det(
      | 5  6  7  |
      | 9  10 11 |
      | 13 14 15 |
    )
    - 4 * det(
      | 1  2  3  |
      | 9  10 11 |
      | 13 14 15 |
    )
    + 8 * det(
      | 1  2  3  |
      | 5  6  7  |
      | 13 14 15 |
    )
    - 12 * det(
      | 1  2  3  |
      | 5  6  7  |
      | 9  10 11 |
    )
  */
  // clang-format off

  auto d_10_11_14_15 = V[10] * V[15] - V[11] * V[14];
  auto d_9_11_13_15  = V[9]  * V[15] - V[11] * V[13];
  auto d_9_10_13_14  = V[9]  * V[14] - V[10] * V[13];
  auto d_5_6_13_14   = V[5]  * V[14] - V[6]  * V[13];
  auto d_5_6_9_10    = V[5]  * V[10] - V[6]  * V[9];
  auto d_5_7_9_11    = V[5]  * V[11] - V[7]  * V[9];
  auto d_5_7_13_15   = V[5]  * V[15] - V[7]  * V[13];
  auto d_6_7_14_15   = V[6]  * V[15] - V[7]  * V[14];
  auto d_6_7_10_11   = V[6]  * V[11] - V[7]  * V[10];

  auto d_5_6_7_9_10_11_13_14_15 = (V[5] * d_10_11_14_15 - V[6] * d_9_11_13_15 + V[7] * d_9_10_13_14);
  auto d_1_2_3_9_10_11_13_14_15 = (V[1] * d_10_11_14_15 - V[2] * d_9_11_13_15 + V[3] * d_9_10_13_14);
  auto d_1_2_3_5_6_7_13_14_15   = (V[1] * d_6_7_14_15   - V[2] * d_5_7_13_15  + V[3] * d_5_6_13_14);
  auto d_1_2_3_5_6_7_9_10_11    = (V[1] * d_6_7_10_11   - V[2] * d_5_7_9_11   + V[3] * d_5_6_9_10);

  return
    +V[0]  * d_5_6_7_9_10_11_13_14_15
    -V[4]  * d_1_2_3_9_10_11_13_14_15
    +V[8]  * d_1_2_3_5_6_7_13_14_15
    -V[12] * d_1_2_3_5_6_7_9_10_11;
  // clang-format on
}

template <typename T>
bool matrix4<T>::inverted(matrix4 &inverse) const
{
  inverse = *this;
  bool is_invertible = inverse.invert();
  return is_invertible;
}

template <typename T>
bool matrix4<T>::invert()
{
  if (is_identity())
    return true;

  // Note:
  // Copy-pasted from determinant() method above
  // clang-format off
  auto d_10_11_14_15 = V[10] * V[15] - V[11] * V[14];
  auto d_9_11_13_15  = V[9]  * V[15] - V[11] * V[13];
  auto d_9_10_13_14  = V[9]  * V[14] - V[10] * V[13];
  auto d_5_6_13_14   = V[5]  * V[14] - V[6]  * V[13];
  auto d_5_6_9_10    = V[5]  * V[10] - V[6]  * V[9];
  auto d_5_7_9_11    = V[5]  * V[11] - V[7]  * V[9];
  auto d_5_7_13_15   = V[5]  * V[15] - V[7]  * V[13];
  auto d_6_7_14_15   = V[6]  * V[15] - V[7]  * V[14];
  auto d_6_7_10_11   = V[6]  * V[11] - V[7]  * V[10];

  auto d_5_6_7_9_10_11_13_14_15 = (V[5] * d_10_11_14_15 - V[6] * d_9_11_13_15 + V[7] * d_9_10_13_14);
  auto d_1_2_3_9_10_11_13_14_15 = (V[1] * d_10_11_14_15 - V[2] * d_9_11_13_15 + V[3] * d_9_10_13_14);
  auto d_1_2_3_5_6_7_13_14_15   = (V[1] * d_6_7_14_15   - V[2] * d_5_7_13_15  + V[3] * d_5_6_13_14);
  auto d_1_2_3_5_6_7_9_10_11    = (V[1] * d_6_7_10_11   - V[2] * d_5_7_9_11   + V[3] * d_5_6_9_10);

  f64 d =
    +V[0]  * d_5_6_7_9_10_11_13_14_15
    -V[4]  * d_1_2_3_9_10_11_13_14_15
    +V[8]  * d_1_2_3_5_6_7_13_14_15
    -V[12] * d_1_2_3_5_6_7_9_10_11;

  if (rt::is_0(d))
    return false;

  /*

 | 0  1  2  3  |
 | 4  5  6  7  |
 | 8  9  10 11 |
 | 12 13 14 15 |

 */

  matrix4 t(*this);
  d = rt::recp(d);

  auto d_8_11_12_15 = V[8] * V[15] - V[11] * V[12];
  auto d_8_10_12_14 = V[8] * V[14] - V[10] * V[12];
  auto d_8_9_12_13  = V[8] * V[13] - V[9]  * V[12];
  auto d_4_6_12_14  = V[4] * V[14] - V[6]  * V[12];
  auto d_4_7_12_15  = V[4] * V[15] - V[7]  * V[12];
  auto d_4_5_12_13  = V[4] * V[13] - V[5]  * V[12];
  auto d_4_7_8_11   = V[4] * V[11] - V[7]  * V[8];
  auto d_4_6_8_10   = V[4] * V[10] - V[6]  * V[8];
  auto d_4_5_8_9    = V[4] * V[9]  - V[5]  * V[8];

  /*        assign and transpose in-place         */
  /*  0  */ V[0]  = d *  d_5_6_7_9_10_11_13_14_15;
  /*  1  */ V[4]  = d * -(t.V[4] * d_10_11_14_15 - t.V[6] * d_8_11_12_15 + t.V[7] * d_8_10_12_14);
  /*  2  */ V[8]  = d *  (t.V[4] * d_9_11_13_15  - t.V[5] * d_8_11_12_15 + t.V[7] * d_8_9_12_13 );
  /*  3  */ V[12] = d * -(t.V[4] * d_9_10_13_14  - t.V[5] * d_8_10_12_14 + t.V[6] * d_8_9_12_13 );
  /*  4  */ V[1]  = d * -d_1_2_3_9_10_11_13_14_15;
  /*  5  */ V[5]  = d *  (t.V[0] * d_10_11_14_15 - t.V[2] * d_8_11_12_15 + t.V[3] * d_8_10_12_14);
  /*  6  */ V[9]  = d * -(t.V[0] * d_9_11_13_15  - t.V[1] * d_8_11_12_15 + t.V[3] * d_8_9_12_13 );
  /*  7  */ V[7]  = d *  -(t.V[0] * d_9_10_13_14 - t.V[1] * d_8_10_12_14 + t.V[2] * d_8_9_12_13 ); // why minus
  /*  8  */ V[2]  = d *  d_1_2_3_5_6_7_13_14_15;
  /*  9  */ V[6]  = d *  (t.V[0] * d_6_7_14_15   - t.V[2] * d_4_7_12_15  + t.V[3] * d_4_6_12_14 );
  /* 10  */ V[10] = d *  (t.V[0] * d_5_7_13_15   - t.V[1] * d_4_7_12_15  + t.V[3] * d_4_5_12_13 );
  /* 11  */ V[14] = d * -(t.V[0] * d_5_6_13_14   - t.V[1] * d_4_6_12_14  + t.V[2] * d_4_5_12_13 );
  /* 12  */ V[3]  = d * -d_1_2_3_5_6_7_9_10_11;
  /* 13  */ V[13] = d * -(t.V[0] * d_6_7_10_11   - t.V[2] * d_4_7_8_11   + t.V[3] * d_4_6_8_10  ); // why minus
  /* 14  */ V[11] = d * -(t.V[0] * d_5_7_9_11    - t.V[1] * d_4_7_8_11   + t.V[3] * d_4_5_8_9   );
  /* 15  */ V[15] = d *  (t.V[0] * d_5_6_9_10    - t.V[1] * d_4_6_8_10   + t.V[2] * d_4_5_8_9   );

  // clang-format on
  return true;
}

template <typename T>
matrix4<T> matrix4<T>::transposed() const
{
  matrix4 t(*this);
  t.transpose();
  return t;
}

template <typename T>
matrix4<T> &matrix4<T>::transpose()
{
  /*
  | 0  1  2  3  |    | 0  4  8  12 |
  | 4  5  6  7  |    | 1  5  9  13 |
  | 8  9  10 11 | -> | 2  6  10 14 |
  | 12 13 14 15 |    | 3  7  11 15 |

  */
  std::swap(V[1], V[4]);
  std::swap(V[2], V[8]);
  std::swap(V[3], V[12]);
  std::swap(V[6], V[9]);
  std::swap(V[7], V[13]);
  std::swap(V[11], V[14]);
  return *this;
}

template <typename T>
bool matrix4<T>::is_identity() const
{
  // clang-format off
  return rt::eq(V[0], 1) && rt::eq(V[5], 1) && rt::eq(V[10], 1) && rt::eq(V[15], 1) &&
         rt::is_0(V[1])  && rt::is_0(V[2])  && rt::is_0(V[3])   && rt::is_0(V[4])   &&
         rt::is_0(V[6])  && rt::is_0(V[7])  && rt::is_0(V[8])   && rt::is_0(V[9])   &&
         rt::is_0(V[11]) && rt::is_0(V[12]) && rt::is_0(V[13])  && rt::is_0(V[14]);
  // clang-format on
}

template <typename T>
void matrix4<T>::set_all(T value)
{
  std::memset(begin(), value, 16 * sizeof(T));
}

template <typename T>
void matrix4<T>::fill_data(const T *ptr)
{
  std::copy_n(ptr, 16, begin());
}

template <typename T>
void matrix4<T>::fill_with_il(std::initializer_list<T> il)
{
  std::copy_n(il.begin(), std::min<unsigned long>(il.size(), 16), begin());
  if (il.size() < 16) {
    std::memset(begin() + il.size(), 0, (16 - il.size()) * sizeof(T));
  }
}

template <typename T>
void matrix4<T>::handle_init_style_op(matrix4_init_style_op op)
{
  switch (op) {
    case matrix4_init_style_op::RT_MAT_INIT_NOTHING:
      break;
    case matrix4_init_style_op::RT_MAT_INIT_INVERTED:
      if (!invert()) {
        set_all(T(0));
      }
      break;
    case matrix4_init_style_op::RT_MAT_INIT_TRANSPOSED:
      transpose();
      break;
    case matrix4_init_style_op::RT_MAT_INIT_INVERSE_TRANSPOSED:
      if (!invert()) {
        set_all(T(0));
        break;
      }
      transpose();
      break;
    default:
      break;
  }
}

#endif  // RT_MATH_MATRIX_4_INL
