#ifndef RT_MATH_QUATERNION_HPP
#define RT_MATH_QUATERNION_HPP
#pragma once

#include <raytracer/engine/typedefs.hpp>
#include <raytracer/math/vector3.hpp>
#include <raytracer/math/matrix4.hpp>

namespace rt {

enum quaternion_init_style_op {
  RT_QUAT_INIT_NOTHING,   //
  RT_QUAT_INIT_INVERTED,  //
};

class quaternion {
 private:
  std::array<f64, 4> V;

 public:
  explicit quaternion(f64 w = 1, f64 x = 0, f64 y = 0, f64 z = 0);
  quaternion(std::initializer_list<f64> il, quaternion_init_style_op op = RT_QUAT_INIT_NOTHING);
  quaternion(const quaternion &other, quaternion_init_style_op op = RT_QUAT_INIT_NOTHING);
  template <typename T>
  quaternion(const matrix4<T> &matrix, quaternion_init_style_op op = RT_QUAT_INIT_NOTHING);
  template <typename T>
  quaternion(const vector3<T> &other, quaternion_init_style_op op = RT_QUAT_INIT_NOTHING);
  quaternion(quaternion &&other, quaternion_init_style_op op = RT_QUAT_INIT_NOTHING) noexcept;
  ~quaternion();

  static quaternion euler(const f64 x, const f64 y, const f64 z);

  [[nodiscard]] bool operator=(const quaternion &rhs);
  [[nodiscard]] bool operator=(quaternion &&rhs) noexcept;
  template <typename T>
  [[nodiscard]] bool operator=(const matrix4<T> &rhs) noexcept;

  [[nodiscard]] bool operator==(const quaternion &rhs) const;
  [[nodiscard]] bool operator!=(const quaternion &rhs) const;

  [[nodiscard]] quaternion operator*(const quaternion &other) const;
  quaternion &operator*=(const quaternion &other);

  [[nodiscard]] bool inverted() const;
  [[nodiscard]] bool invert();

  [[nodiscard]] f64 dot(const quaternion &other) const;

 private:
  void handle_init_style_op(quaternion_init_style_op op);
};

}  // namespace rt

#include "inls.hpp"
#endif  // RT_MATH_QUATERNION_HPP