#ifndef RT_MATH_VECTOR_3_HPP
#define RT_MATH_VECTOR_3_HPP
#pragma once

#include <raytracer/engine/typedefs.hpp>
#include <raytracer/math/math_ops.hpp>

namespace rt {

enum vector3_init_style_op {
  RT_VEC_INIT_NOTHING,
  RT_VEC_INIT_NEGATED,
  RT_VEC_INIT_NORMALIZED,
};

template <typename T>
class vector3 {
 private:
  std::array<T, 3> V;

 public:
  T &x = V[0], &y = V[1], &z = V[2];
  T &r = V[0], &g = V[1], &b = V[2];

  explicit vector3(T x = T(0), T y = T(0), T z = T(0));
  explicit vector3(std::array<T, 3> arr, vector3_init_style_op op = RT_VEC_INIT_NOTHING);
  vector3(std::initializer_list<T> il, vector3_init_style_op op = RT_VEC_INIT_NOTHING);
  vector3(const vector3 &other, vector3_init_style_op op = RT_VEC_INIT_NOTHING);
  vector3(vector3 &&other, vector3_init_style_op op = RT_VEC_INIT_NOTHING) noexcept;
  ~vector3();

  static vector3 unit_x();
  static vector3 unit_y();
  static vector3 unit_z();

  const T *begin() const;
  T *begin();
  const T *end() const;
  T *end();

  const T &operator[](size_t index) const;
  T &operator[](size_t index);

  vector3 &operator=(const vector3 &other);
  vector3 &operator=(vector3 &&other) noexcept;
  vector3 &operator=(std::initializer_list<T> il);
  vector3 &operator=(const std::array<T, 3> &array);

  bool operator==(const vector3 &rhs) const;
  bool operator!=(const vector3 &rhs) const;

  vector3 operator-() const;

  vector3 operator+(const vector3 &other) const;
  vector3 &operator+=(const vector3 &other);
  template <typename Scalar>
  vector3 operator+(Scalar scalar) const;
  template <typename Scalar>
  vector3 &operator+=(Scalar scalar);

  vector3 operator-(const vector3 &other) const;
  vector3 &operator-=(const vector3 &other);
  template <typename Scalar>
  vector3 operator-(Scalar scalar) const;
  template <typename Scalar>
  vector3 &operator-=(Scalar scalar);

  vector3 operator*(const vector3 &other) const;
  vector3 &operator*=(const vector3 &other);
  template <typename Scalar>
  vector3 operator*(Scalar scalar) const;
  template <typename Scalar>
  vector3 &operator*=(Scalar scalar);

  [[nodiscard]] bool is_normalized() const;
  [[nodiscard]] bool is_zero() const;
  [[nodiscard]] f64 magnitude_sqr() const;
  [[nodiscard]] f64 magnitude() const;
  [[nodiscard]] f64 dot(const vector3 &other) const;
  [[nodiscard]] vector3 cross(const vector3 &other) const;

  [[nodiscard]] vector3 normalized() const;
  vector3 &normalize();

 private:
  void set_all(T value);
  void fill_data(const T *ptr);
  void fill_with_il(std::initializer_list<T> il);
  void handle_init_style_op(vector3_init_style_op op);
};

}  // namespace rt

#include "inls.hpp"

#endif  // RT_MATH_VECTOR_3_HPP
