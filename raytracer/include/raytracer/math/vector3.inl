#ifndef RT_MATH_VECTOR_3_INL
#define RT_MATH_VECTOR_3_INL
#pragma once

#include "vector3.hpp"

using namespace rt;

template <typename T>
vector3<T>::vector3(T x, T y, T z)
{
  V[0] = x;
  V[1] = y;
  V[2] = z;
}

template <typename T>
vector3<T>::vector3(std::array<T, 3> arr, vector3_init_style_op op)
{
  fill_data(arr.begin());
  handle_init_style_op(op);
}

template <typename T>
vector3<T>::vector3(std::initializer_list<T> il, vector3_init_style_op op)
{
  fill_with_il(il);
  handle_init_style_op(op);
}

template <typename T>
vector3<T>::vector3(const vector3 &other, vector3_init_style_op op)
{
  fill_data(other.begin());
  handle_init_style_op(op);
}

template <typename T>
vector3<T>::vector3(vector3 &&other, vector3_init_style_op op) noexcept
{
  std::move(other.begin(), other.end(), begin());
  handle_init_style_op(op);
}

template <typename T>
vector3<T>::~vector3() = default;

template <typename T>
vector3<T> vector3<T>::unit_x()
{
  return vector3(T(1));
}

template <typename T>
vector3<T> vector3<T>::unit_y()
{
  return vector3(T(0), T(1));
}

template <typename T>
vector3<T> vector3<T>::unit_z()
{
  return vector3(T(0), T(0), T(1));
}

template <typename T>
const T *vector3<T>::begin() const
{
  return V.begin();
}

template <typename T>
T *vector3<T>::begin()
{
  return V.begin();
}

template <typename T>
const T *vector3<T>::end() const
{
  return V.end();
}

template <typename T>
T *vector3<T>::end()
{
  return V.end();
}

template <typename T>
const T &vector3<T>::operator[](size_t index) const
{
  return V[index];
}

template <typename T>
T &vector3<T>::operator[](size_t index)
{
  return V[index];
}

template <typename T>
vector3<T> &vector3<T>::operator=(const vector3 &other)
{
  fill_data(other.begin());
  return *this;
}

template <typename T>
vector3<T> &vector3<T>::operator=(vector3 &&other) noexcept
{
  std::move(other.begin(), other.end(), begin());
  return *this;
}

template <typename T>
vector3<T> &vector3<T>::operator=(std::initializer_list<T> il)
{
  fill_with_il(il);
  return *this;
}

template <typename T>
vector3<T> &vector3<T>::operator=(const std::array<T, 3> &array)
{
  fill_data(array.begin());
  return *this;
}

template <typename T>
bool vector3<T>::operator==(const vector3 &rhs) const
{
  if (this == &rhs)
    return true;
  return std::equal(begin(), end(), rhs.begin(), [](const T &t1, const T &t2) -> bool {  //
    return rt::eq<T>(t1, t2, floating_prec_low);                                         //
  });                                                                                    //
}

template <typename T>
bool vector3<T>::operator!=(const vector3 &rhs) const
{
  return !(*this == rhs);
}

template <typename T>
vector3<T> vector3<T>::operator-() const
{
  return vector3(-x, -y, -z);
}

template <typename T>
vector3<T> vector3<T>::operator+(const vector3 &other) const
{
  vector3 t(*this);
  t += other;
  return t;
}

template <typename T>
vector3<T> &vector3<T>::operator+=(const vector3 &other)
{
  x += other.x;
  y += other.y;
  z += other.z;
  return *this;
}

template <typename T>
template <typename Scalar>
vector3<T> vector3<T>::operator+(Scalar scalar) const
{
  vector3 t(*this);
  t += T(scalar);
  return t;
}

template <typename T>
template <typename Scalar>
vector3<T> &vector3<T>::operator+=(Scalar scalar)
{
  x += T(scalar);
  y += T(scalar);
  z += T(scalar);
  return *this;
}

template <typename T>
vector3<T> vector3<T>::operator-(const vector3 &other) const
{
  vector3 t(*this);
  t -= other;
  return t;
}

template <typename T>
vector3<T> &vector3<T>::operator-=(const vector3 &other)
{
  x -= other.x;
  y -= other.y;
  z -= other.z;
  return *this;
}

template <typename T>
template <typename Scalar>
vector3<T> vector3<T>::operator-(Scalar scalar) const
{
  vector3 t(*this);
  t -= T(scalar);
  return t;
}

template <typename T>
template <typename Scalar>
vector3<T> &vector3<T>::operator-=(Scalar scalar)
{
  x -= T(scalar);
  y -= T(scalar);
  z -= T(scalar);
  return *this;
}

template <typename T>
vector3<T> vector3<T>::operator*(const vector3 &other) const
{
  vector3 t(*this);
  t *= other;
  return t;
}

template <typename T>
vector3<T> &vector3<T>::operator*=(const vector3 &other)
{
  x *= other.x;
  y *= other.y;
  z *= other.z;
  return *this;
}

template <typename T>
template <typename Scalar>
vector3<T> vector3<T>::operator*(Scalar scalar) const
{
  vector3 t(*this);
  t *= T(scalar);
  return t;
}

template <typename T>
template <typename Scalar>
vector3<T> &vector3<T>::operator*=(Scalar scalar)
{
  x *= T(scalar);
  y *= T(scalar);
  z *= T(scalar);
  return *this;
}

template <typename T>
bool vector3<T>::is_normalized() const
{
  return rt::eq(magnitude_sqr(), 1);
}

template <typename T>
bool vector3<T>::is_zero() const
{
  return rt::is_0(magnitude_sqr());
}

template <typename T>
f64 vector3<T>::magnitude_sqr() const
{
  return x * x + y * y + z * z;
}

template <typename T>
f64 vector3<T>::magnitude() const
{
  return rt::sqrt(magnitude_sqr());
}

template <typename T>
f64 vector3<T>::dot(const vector3 &other) const
{
  return x * other.x + y * other.y + z * other.z;
}

template <typename T>
vector3<T> vector3<T>::cross(const vector3 &other) const
{
  vector3 t(*this);
  t.x = y * other.z - z * other.y;
  t.y = z * other.x - x * other.z;
  t.z = x * other.y - y * other.x;
  return t;
}

template <typename T>
vector3<T> vector3<T>::normalized() const
{
  vector3 t(*this);
  if (!is_zero())
    t.normalize();
  return t;
}

template <typename T>
vector3<T> &vector3<T>::normalize()
{
  auto m = magnitude();
  if (rt::is_0(m))
    return *this;
  x /= m;
  y /= m;
  z /= m;
  return *this;
}

template <typename T>
void vector3<T>::set_all(T value)
{
  std::memset(begin(), value, 4 * sizeof(T));
}

template <typename T>
void vector3<T>::fill_data(const T *ptr)
{
  std::copy_n(ptr, 4, begin());
}

template <typename T>
void vector3<T>::fill_with_il(std::initializer_list<T> il)
{
  std::copy_n(il.begin(), std::min<unsigned long>(il.size(), 4), begin());
  if (il.size() < 4) {
    std::memset(begin() + il.size(), 0, (4 - il.size()) * sizeof(T));
  }
}

template <typename T>
void vector3<T>::handle_init_style_op(vector3_init_style_op op)
{
  switch (op) {
    case vector3_init_style_op::RT_VEC_INIT_NOTHING:
      break;
    case vector3_init_style_op::RT_VEC_INIT_NEGATED:
      *this *= T(-1);
      break;
    case vector3_init_style_op::RT_VEC_INIT_NORMALIZED:
      normalize();
      break;
  }
}

#endif  // RT_MATH_VECTOR_3_INL