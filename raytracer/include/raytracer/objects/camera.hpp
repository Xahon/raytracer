#ifndef RT_OBJECTS_CAMERA_HPP
#define RT_OBJECTS_CAMERA_HPP
#pragma once

#include <raytracer/engine/typedefs.hpp>
#include <raytracer/objects/object.hpp>

namespace rt {

class camera : public rt::object {
 private:
  f32 fov = 60.0f;
  f32 near = 0.01f;
  f32 far = 3000.0f;
  f32 aspect;
  bool is_dirty = true;

  glm::mat4 view_matrix;
  glm::mat4 projection_matrix;
  glm::mat4 view_projection_matrix;

 public:
  explicit camera(f32 aspect, f32 fov = 60.0f, f32 near = 0.01f, f32 far = 3000.0f);
  camera(const rt::camera &other);
  ~camera() override;

  [[nodiscard]] f32 get_fov() const;
  [[nodiscard]] f32 get_near() const;
  [[nodiscard]] f32 get_far() const;
  [[nodiscard]] f32 get_aspect() const;

  void set_fov(f32 fov);
  void set_near(f32 near);
  void set_far(f32 far);
  void set_aspect(f32 aspect);

  const glm::mat4 &get_or_calc_view_matrix();
  const glm::mat4 &get_or_calc_projection_matrix();
  const glm::mat4 &get_or_calc_view_projection_matrix();

  void recalculate_matrices();
};

}  // namespace rt

#endif  // RT_OBJECTS_CAMERA_HPP
