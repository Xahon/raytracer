#ifndef RT_OBJECTS_GEOMETRY_HPP
#define RT_OBJECTS_GEOMETRY_HPP
#pragma once

#include <raytracer/engine/typedefs.hpp>

namespace rt {

class geometry {
 private:
  std::vector<glm::vec<3, rt::fl_t>> vertices;
  std::vector<glm::vec<3, rt::fl_t>> normals;
  std::vector<glm::vec<2, rt::fl_t>> uv;
  std::vector<rt::idx_t> indices;

 public:
  geometry();
  geometry(const rt::geometry &other);
  geometry(rt::geometry &&other) noexcept;
  virtual ~geometry();

  [[nodiscard]] const std::vector<glm::vec<3, rt::fl_t>> &get_vertices() const;
  [[nodiscard]] const std::vector<glm::vec<3, rt::fl_t>> &get_normals() const;
  [[nodiscard]] const std::vector<glm::vec<2, rt::fl_t>> &get_uv() const;
  [[nodiscard]] const std::vector<rt::idx_t> &get_indices() const;

  void set_vertices(const std::vector<glm::vec<3, rt::fl_t>> &vertices);
  void set_normals(const std::vector<glm::vec<3, rt::fl_t>> &normals);
  void set_uv(const std::vector<glm::vec<2, rt::fl_t>> &uv);
  void set_indices(const std::vector<rt::idx_t> &indices);
};

}  // namespace rt

#endif  // RT_OBJECTS_GEOMETRY_HPP
