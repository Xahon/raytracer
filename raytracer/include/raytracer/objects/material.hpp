#ifndef RT_OBJECTS_MATERIAL_HPP
#define RT_OBJECTS_MATERIAL_HPP
#pragma once

#include <raytracer/engine/typedefs.hpp>

namespace rt {

class material {
 public:
  static const char *default_vertex_shader;
  static const char *default_fragment_shader;

 protected:
  char *vertex_shader{};
  char *fragment_shader{};

 private:
  GLuint program_id{0};
  GLuint vertex_shader_id{0};
  GLuint fragment_shader_id{0};

 public:
  material();
  material(const char *vertex_shader_source, const char *fragment_shader_source, bool is_file);
  material(const rt::material &other);
  material(rt::material &&other) noexcept;
  virtual ~material();

  rt::material &operator=(const rt::material &other);
  rt::material &operator=(rt::material &&other) noexcept;

  [[nodiscard]] const char *get_vertex_shader() const;
  [[nodiscard]] const char *get_fragment_shader() const;
  [[nodiscard]] GLuint get_program_id() const;
  [[nodiscard]] GLuint get_vertex_shader_id() const;
  [[nodiscard]] GLuint get_fragment_shader_id() const;

  void compile();
};

}  // namespace rt

#endif  // RT_OBJECTS_MATERIAL_HPP