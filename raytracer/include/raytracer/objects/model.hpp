#ifndef RT_OBJECTS_MODEL_HPP
#define RT_OBJECTS_MODEL_HPP
#pragma once

#include <raytracer/engine/typedefs.hpp>
#include <raytracer/objects/object.hpp>
#include <raytracer/objects/material.hpp>

namespace rt {

class model : public rt::object {
 private:
  rt::geometry geometry;
  rt::material material;

  glm::mat4 view_matrix;
  glm::mat4 projection_matrix;
  glm::mat4 view_projection_matrix;

  bool is_material_dirty = true;
  rt::idx_t indices_length = 0;
  rt::idx_t VAO{};
  std::array<rt::idx_t, 7> VBO{};

 public:
  model(rt::geometry geometry, rt::material material);
  model(const rt::model &other);
  model(rt::model &&other) noexcept;
  ~model() override;

  [[nodiscard]] const rt::material &get_material() const;
  void set_material(const rt::material &material);

  void update_camera_matrices(const glm::mat4 &v, const glm::mat4 &p, const glm::mat4 &vp);

  void start() override;
  void render(const rt::dur_s &delta_time) override;

 private:
  void update_material_shader_program();
  void render_tris();
};

}  // namespace rt

#endif  // RT_OBJECTS_MODEL_HPP
