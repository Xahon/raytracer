#ifndef RT_OBJECTS_OBJECT_HPP
#define RT_OBJECTS_OBJECT_HPP
#pragma once

#include <raytracer/engine/typedefs.hpp>
#include <raytracer/objects/geometry.hpp>
#include <raytracer/objects/unique_id.hpp>
#include <raytracer/engine/lifecycle.hpp>

namespace rt {

class object : public rt::unique_id, public rt::lifecycle {
 private:
  glm::mat4 model_matrix = glm::mat4(1);
  glm::vec3 position = glm::vec3(0);
  glm::mat4 rotation = glm::mat4(1);
  glm::vec3 scale = glm::vec3(1, 1, 1);

  bool is_dirty = true;

 public:
  object();
  ~object() override;

  [[nodiscard]] const glm::mat4 &get_or_calc_model_matrix();
  [[nodiscard]] const glm::vec3 &get_position() const;
  [[nodiscard]] const glm::mat4 &get_rotation() const;
  [[nodiscard]] const glm::vec3 &get_scale() const;

  void set_position(const glm::vec3 &position);
  void set_position(f32 x, f32 y, f32 z);
  void set_rotation(const glm::mat4 &rotation_matrix);
  void set_scale(const glm::vec3 &scale);
  void set_scale(f32 x, f32 y, f32 z);
};

}  // namespace rt

#endif  // RT_OBJECTS_OBJECT_HPP
