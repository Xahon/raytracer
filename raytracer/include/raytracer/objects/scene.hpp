#ifndef RT_OBJECTS_SCENE_HPP
#define RT_OBJECTS_SCENE_HPP
#pragma once

#include <raytracer/engine/typedefs.hpp>
#include <raytracer/objects/camera.hpp>
#include <raytracer/engine/lifecycle.hpp>
#include <raytracer/objects/unique_id.hpp>
#include <raytracer/objects/object.hpp>

namespace rt {

class scene : public rt::lifecycle {
 private:
  std::map<size_t, rt::object *> objects;
  rt::camera *active_camera = nullptr;

 public:
  scene();
  virtual ~scene();

  void add_object(rt::object *object);
  void remove_object(rt::unique_id *id);

  void start() override;
  void update(const rt::dur_s &delta_time) override;
  void render(const rt::dur_s &delta_time) override;

  void set_camera(rt::camera *camera);
};

}  // namespace rt

#endif  // RT_OBJECTS_SCENE_HPP
