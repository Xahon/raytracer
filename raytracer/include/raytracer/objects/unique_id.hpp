#ifndef RT_OBJECTS_UNIQUE_ID_HPP
#define RT_OBJECTS_UNIQUE_ID_HPP
#pragma once

#include <raytracer/engine/typedefs.hpp>

namespace rt {

class unique_id {
 protected:
  unique_id();

 public:
  virtual ~unique_id();

  [[nodiscard]] size_t get_id() const;

 private:
  size_t id;
};

}  // namespace rt

#endif  // RT_OBJECTS_UNIQUE_ID_HPP
