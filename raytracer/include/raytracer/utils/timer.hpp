#ifndef RT_UTILS_TIMER_HPP
#define RT_UTILS_TIMER_HPP
#pragma once

#include <raytracer/engine/typedefs.hpp>

namespace rt {

class timer {
 public:
  static const int timer_default_tick_interval_ms = 100;

 private:
  rt::dur_s time_from_start;
  std::thread thread;
  int tick_interval;
  int ticks = 0;
  bool is_paused = false;
  bool is_stopped = false;
  bool thread_created = false;

  void create_thread_if_not_exists();

 protected:
  virtual void on_tick();

 public:
  explicit timer(int tick_interval = timer::timer_default_tick_interval_ms);
  virtual ~timer();

  void start();
  void pause(bool pause_enabled = true);
  void stop();

  int get_ticks();
  [[nodiscard]] const dur_s &get_time_from_start() const;
};

}  // namespace rt

#endif  // RT_UTILS_TIMER_HPP
