#ifndef RT_UTILS_UTILS_HPP
#define RT_UTILS_UTILS_HPP
#pragma once

#include <raytracer/engine/typedefs.hpp>

namespace rt::utils {

template <typename T>
struct inner_type {
  typedef void type;
};

template <template <typename, typename...> class Outer, typename T, typename... Args>
struct inner_type<Outer<T, Args...>> {
  typedef T type;
};

template <typename... Args>
using inner_type_t = typename inner_type<Args...>::type;

template <typename T, typename Replacement>
struct replace_inner_type {
  typedef void type;
};

template <template <typename, typename...> class Outer, typename Replacement, typename T, typename... Args>
struct replace_inner_type<Outer<T, Args...>, Replacement> {
  typedef Outer<Replacement, Args...> type;
};

template <typename... Args>
using replace_inner_type_t = typename replace_inner_type<Args...>::type;

template <template <typename, typename...> typename Collection, typename T1, typename T2, typename... Args>
Collection<T2> select(const Collection<T1, Args...> &collection, std::function<T2(const T1 &)> map)
{
  Collection<T2> c;
  for (auto &it : collection) {
    auto newVal = map(it);
    c.push_back(newVal);
  }
  return c;
}

template <template <typename, typename...> typename Collection, typename T1, typename T2, typename... Args>
Collection<T2> aggregate_select(const Collection<T1, Args...> &collection, std::function<void(Collection<T2> &, const T1 &)> agg)
{
  Collection<T2> c;
  for (auto &it : collection) {
    agg(c, it);
  }
  return c;
}

}  // namespace rt::utils

#endif  // RT_UTILS_UTILS_HPP
