#include <raytracer/objects/camera.hpp>

rt::camera::camera(f32 aspect, f32 fov, f32 near, f32 far)
    : fov(fov), near(near), far(far), aspect(aspect), view_matrix(1.0f), projection_matrix(1.0f)
{
}

rt::camera::camera(const camera &other)
    : fov(other.fov),
      near(other.near),
      far(other.far),
      aspect(other.aspect),
      view_matrix(other.view_matrix),
      projection_matrix(other.projection_matrix)
{
}

rt::camera::~camera() = default;

const glm::mat4 &rt::camera::get_or_calc_view_matrix()
{
  if (is_dirty) {
    recalculate_matrices();
    is_dirty = false;
  }
  return view_matrix;
}

const glm::mat4 &rt::camera::get_or_calc_projection_matrix()
{
  if (is_dirty) {
    recalculate_matrices();
    is_dirty = false;
  }
  return projection_matrix;
}

const glm::mat4 &rt::camera::get_or_calc_view_projection_matrix()
{
  if (is_dirty) {
    recalculate_matrices();
    is_dirty = false;
  }
  return view_projection_matrix;
}

rt::f32 rt::camera::get_fov() const
{
  return fov;
}

rt::f32 rt::camera::get_near() const
{
  return near;
}

rt::f32 rt::camera::get_far() const
{
  return far;
}

rt::f32 rt::camera::get_aspect() const
{
  return aspect;
}

void rt::camera::set_fov(rt::f32 fov)
{
  rt::camera::fov = fov;
  is_dirty = true;
}

void rt::camera::set_near(rt::f32 near)
{
  rt::camera::near = near;
  is_dirty = true;
}

void rt::camera::set_far(rt::f32 far)
{
  rt::camera::far = far;
  is_dirty = true;
}

void rt::camera::set_aspect(rt::f32 aspect)
{
  rt::camera::aspect = aspect;
  is_dirty = true;
}

void rt::camera::recalculate_matrices()
{
  view_matrix = object::get_or_calc_model_matrix();
  projection_matrix = glm::perspective(fov, aspect, near, far);
  view_projection_matrix = projection_matrix * view_matrix;
  is_dirty = false;
}
