#include <raytracer/objects/geometry.hpp>

rt::geometry::geometry() = default;

rt::geometry::geometry(const rt::geometry &other) = default;

rt::geometry::geometry(rt::geometry &&other) noexcept
    : vertices(std::move(other.vertices)), normals(std::move(other.normals)), uv(std::move(other.uv)), indices(std::move(other.indices))
{
}

rt::geometry::~geometry() = default;

const std::vector<glm::vec<3, rt::fl_t>> &rt::geometry::get_vertices() const
{
  return vertices;
}

const std::vector<glm::vec<3, rt::fl_t>> &rt::geometry::get_normals() const
{
  return normals;
}

const std::vector<glm::vec<2, rt::fl_t>> &rt::geometry::get_uv() const
{
  return uv;
}

const std::vector<rt::idx_t> &rt::geometry::get_indices() const
{
  return indices;
}

void rt::geometry::set_vertices(const std::vector<glm::vec<3, rt::fl_t>> &vertices)
{
  geometry::vertices = vertices;
}

void rt::geometry::set_normals(const std::vector<glm::vec<3, rt::fl_t>> &normals)
{
  geometry::normals = normals;
}

void rt::geometry::set_uv(const std::vector<glm::vec<2, rt::fl_t>> &uv)
{
  geometry::uv = uv;
}

void rt::geometry::set_indices(const std::vector<rt::idx_t> &indices)
{
  geometry::indices = indices;
}
