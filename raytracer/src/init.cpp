#include <csignal>
#include <raytracer/engine/log.hpp>

void init()
{
  rt::log::init();
  SPDLOG_LOGGER_INFO(rt::log::console, "Init lib\n");
}

void deinit(int signum, const char *name)
{
  if (signum == -1) {
    SPDLOG_LOGGER_INFO(rt::log::console, "Deinit lib\n");
  }
  else {
    SPDLOG_LOGGER_INFO(rt::log::console, "Deinit lib. Signal {} '{}'\n", signum, name);
  }
  exit(1);
}

__attribute__((constructor)) void ctor()
{
  init();

  signal(SIGABRT, [](int signum) -> void { deinit(signum, "SIGABRT"); });
  signal(SIGFPE, [](int signum) -> void { deinit(signum, "SIGFPE"); });
  signal(SIGILL, [](int signum) -> void { deinit(signum, "SIGILL"); });
  signal(SIGINT, [](int signum) -> void { deinit(signum, "SIGINT"); });
  signal(SIGTERM, [](int signum) -> void { deinit(signum, "SIGTERM"); });
  signal(SIGKILL, [](int signum) -> void { deinit(signum, "SIGKILL"); });
  signal(SIGSEGV, [](int signum) -> void { deinit(signum, "SIGSEGV"); });
}

__attribute__((destructor)) void dtor()
{
  deinit(-1, "");
  exit(1);
}