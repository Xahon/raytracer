#include <raytracer/engine/typedefs.hpp>
#include <raytracer/engine/log.hpp>

const char *rt::log::pattern = "|%n| %@ (%!) [%^%l%$] [%H:%M:%S] [p: %P, t: %t] %v";
std::shared_ptr<spdlog::logger> rt::log::console = nullptr;

rt::log::log() = default;

void rt::log::init()
{
  static bool initialized = false;
  if (initialized)
    return;

  spdlog::set_level(spdlog::level::info);
  rt::log::console = spdlog::stdout_color_mt("console");
  rt::log::console->set_pattern(rt::log::pattern);
  initialized = true;
}
