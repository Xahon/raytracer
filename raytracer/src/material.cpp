
#include <raytracer/engine/typedefs.hpp>
#include <raytracer/objects/material.hpp>
#include <raytracer/engine/log.hpp>

const char *rt::material::default_vertex_shader =
"#version 140\n"
"in vec2 position;"
"void main() {"
"  gl_Position = vec4(position.x, position.y, 0, 1 );"
"}";

const char *rt::material::default_fragment_shader =
"#version 140\n"
"out vec4 LFragment;"
"void main() {"
"  LFragment = vec4(1.0, 0.0, 1.0, 1.0);"
"}";

rt::material::material()
    : vertex_shader(const_cast<char *>(rt::material::default_vertex_shader)),
      fragment_shader(const_cast<char *>(rt::material::default_fragment_shader))
{
}

rt::material::material(const char *vertex_shader_source, const char *fragment_shader_source, bool is_file)
{
  if (!is_file) {
    vertex_shader = const_cast<char *>(vertex_shader_source);
    fragment_shader = const_cast<char *>(fragment_shader_source);
    return;
  }

  std::ifstream vss(vertex_shader_source, std::ios::in);
  if (!vss.good()) {
    spdlog::error("File '%s' not found. Using default vertex shader", vertex_shader_source);
    vertex_shader = const_cast<char *>(default_vertex_shader);
  }
  else {
    std::stringstream ss;
    ss << vss.rdbuf();
    vertex_shader = const_cast<char *>(ss.str().c_str());
  }

  std::ifstream fss(fragment_shader_source, std::ios::in);
  if (!fss.good()) {
    spdlog::error("File '%s' not found. Using default fragment shader", fragment_shader_source);
    fragment_shader = const_cast<char *>(default_fragment_shader);
  }
  else {
    std::stringstream ss;
    ss << fss.rdbuf();
    fragment_shader = const_cast<char *>(ss.str().c_str());
  }
}

rt::material::material(const rt::material &other) : vertex_shader(other.vertex_shader), fragment_shader(other.fragment_shader)
{
}

rt::material::material(rt::material &&other) noexcept : vertex_shader(other.vertex_shader), fragment_shader(other.fragment_shader)
{
}

rt::material::~material() = default;

const char *rt::material::get_vertex_shader() const
{
  return vertex_shader;
}

const char *rt::material::get_fragment_shader() const
{
  return fragment_shader;
}

rt::material &rt::material::operator=(const rt::material &other) = default;

rt::material &rt::material::operator=(rt::material &&other) noexcept = default;

void rt::material::compile()
{
  int operation_succeeded;
  char info_log[512];

  vertex_shader_id = glCreateShader(GL_VERTEX_SHADER);
  glShaderSource(vertex_shader_id, 1, &vertex_shader, nullptr);
  glCompileShader(vertex_shader_id);
  glGetShaderiv(vertex_shader_id, GL_COMPILE_STATUS, &operation_succeeded);
  if (!operation_succeeded) {
    glGetShaderInfoLog(vertex_shader_id, sizeof(info_log), nullptr, info_log);
    SPDLOG_LOGGER_CRITICAL(rt::log::console, "Vertex shader compilation failed.\n{}", info_log);
    exit(1);
  }

  fragment_shader_id = glCreateShader(GL_FRAGMENT_SHADER);
  glShaderSource(fragment_shader_id, 1, &fragment_shader, nullptr);
  glCompileShader(fragment_shader_id);
  glGetShaderiv(fragment_shader_id, GL_COMPILE_STATUS, &operation_succeeded);
  if (!operation_succeeded) {
    glGetShaderInfoLog(fragment_shader_id, sizeof(info_log), nullptr, info_log);
    SPDLOG_LOGGER_CRITICAL(rt::log::console, "Fragment shader compilation failed.\n{}", info_log);
  }

  program_id = glCreateProgram();
  glAttachShader(program_id, vertex_shader_id);
  glAttachShader(program_id, fragment_shader_id);
  glLinkProgram(program_id);
  glGetProgramiv(program_id, GL_LINK_STATUS, &operation_succeeded);
  if (!operation_succeeded) {
    glGetProgramInfoLog(program_id, sizeof(info_log), nullptr, info_log);
    SPDLOG_LOGGER_CRITICAL(rt::log::console, "Shaders linking failed.\n{}", info_log);
    exit(1);
  }
  glDeleteShader(vertex_shader_id);
  glDeleteShader(fragment_shader_id);

  SPDLOG_LOGGER_INFO(rt::log::console, "Material shader compiled");
}

GLuint rt::material::get_program_id() const
{
  return program_id;
}

GLuint rt::material::get_vertex_shader_id() const
{
  return vertex_shader_id;
}

GLuint rt::material::get_fragment_shader_id() const
{
  return fragment_shader_id;
}
