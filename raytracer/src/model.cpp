#include <raytracer/objects/model.hpp>
#include <raytracer/engine/log.hpp>
#include <raytracer/utils/utils.hpp>

rt::model::model(rt::geometry geometry, rt::material material) : geometry(std::move(geometry)), material(std::move(material))
{
}

rt::model::model(const rt::model &other) : rt::model(other.geometry, other.material)
{
}

rt::model::model(rt::model &&other) noexcept : rt::model(other.geometry, other.material)
{
}

rt::model::~model()
{
  glDeleteVertexArrays(1, &VAO);
  glDeleteBuffers(VBO.size(), VBO.data());
}

const rt::material &rt::model::get_material() const
{
  return material;
}

void rt::model::set_material(const rt::material &material)
{
  model::material = material;
  is_material_dirty = true;
}

void rt::model::start()
{
}

void rt::model::render(const rt::dur_s &delta_time)
{
  if (is_material_dirty) {
    update_material_shader_program();
    is_material_dirty = false;
  }

  render_tris();
}

void rt::model::render_tris()
{
  glUseProgram(material.get_program_id());

  glBindVertexArray(VAO);

  int vboIdx = -1;
  int attrIdx = -1;

  glBindBuffer(GL_ARRAY_BUFFER, VBO[++vboIdx]);
  glEnableVertexAttribArray(++attrIdx);
  glVertexAttribPointer(attrIdx, 3, GL_FLOAT, GL_FALSE, 3 * sizeof(rt::fl_t), nullptr);

  glBindBuffer(GL_ARRAY_BUFFER, VBO[++vboIdx]);
  glEnableVertexAttribArray(++attrIdx);
  glVertexAttribPointer(attrIdx, 3, GL_FLOAT, GL_FALSE, 3 * sizeof(rt::fl_t), nullptr);

  glBindBuffer(GL_ARRAY_BUFFER, VBO[++vboIdx]);
  glEnableVertexAttribArray(++attrIdx);
  glVertexAttribPointer(attrIdx, 4, GL_FLOAT, GL_FALSE, 16 * sizeof(rt::fl_t), reinterpret_cast<void *>(0 * sizeof(rt::fl_t)));
  glEnableVertexAttribArray(++attrIdx);
  glVertexAttribPointer(attrIdx, 4, GL_FLOAT, GL_FALSE, 16 * sizeof(rt::fl_t), reinterpret_cast<void *>(4 * sizeof(rt::fl_t)));
  glEnableVertexAttribArray(++attrIdx);
  glVertexAttribPointer(attrIdx, 4, GL_FLOAT, GL_FALSE, 16 * sizeof(rt::fl_t), reinterpret_cast<void *>(8 * sizeof(rt::fl_t)));
  glEnableVertexAttribArray(++attrIdx);
  glVertexAttribPointer(attrIdx, 4, GL_FLOAT, GL_FALSE, 16 * sizeof(rt::fl_t), reinterpret_cast<void *>(12 * sizeof(rt::fl_t)));
  glVertexAttribDivisor(attrIdx - 3, vboIdx);
  glVertexAttribDivisor(attrIdx - 2, vboIdx);
  glVertexAttribDivisor(attrIdx - 1, vboIdx);
  glVertexAttribDivisor(attrIdx - 0, vboIdx);

  glBindBuffer(GL_ARRAY_BUFFER, VBO[++vboIdx]);
  glEnableVertexAttribArray(++attrIdx);
  glVertexAttribPointer(attrIdx, 4, GL_FLOAT, GL_FALSE, 16 * sizeof(rt::fl_t), reinterpret_cast<void *>(0 * sizeof(rt::fl_t)));
  glEnableVertexAttribArray(++attrIdx);
  glVertexAttribPointer(attrIdx, 4, GL_FLOAT, GL_FALSE, 16 * sizeof(rt::fl_t), reinterpret_cast<void *>(4 * sizeof(rt::fl_t)));
  glEnableVertexAttribArray(++attrIdx);
  glVertexAttribPointer(attrIdx, 4, GL_FLOAT, GL_FALSE, 16 * sizeof(rt::fl_t), reinterpret_cast<void *>(8 * sizeof(rt::fl_t)));
  glEnableVertexAttribArray(++attrIdx);
  glVertexAttribPointer(attrIdx, 4, GL_FLOAT, GL_FALSE, 16 * sizeof(rt::fl_t), reinterpret_cast<void *>(12 * sizeof(rt::fl_t)));
  glVertexAttribDivisor(attrIdx - 3, vboIdx);
  glVertexAttribDivisor(attrIdx - 2, vboIdx);
  glVertexAttribDivisor(attrIdx - 1, vboIdx);
  glVertexAttribDivisor(attrIdx - 0, vboIdx);

  //
  //  glBindBuffer(GL_ARRAY_BUFFER, VBO[++vboIdx]);
  //  glEnableVertexAttribArray(vboIdx);
  //  glVertexAttribPointer(vboIdx, 16, GL_FLOAT, GL_FALSE, 16 * sizeof(rt::fl_t), nullptr);
  //
  //  glBindBuffer(GL_ARRAY_BUFFER, VBO[++vboIdx]);
  //  glEnableVertexAttribArray(vboIdx);
  //  glVertexAttribPointer(vboIdx, 16, GL_FLOAT, GL_FALSE, 16 * sizeof(rt::fl_t), nullptr);

  glDrawElements(GL_TRIANGLES, indices_length, GL_UNSIGNED_INT, nullptr);

  for (int j = attrIdx; j >= 0; --j) {
    glDisableVertexAttribArray(j);
  }

  glBindBuffer(GL_ARRAY_BUFFER, 0);
  glBindVertexArray(0);
}

void rt::model::update_material_shader_program()
{
  material.compile();

  auto &vertices = geometry.get_vertices();
  auto &normals = geometry.get_normals();
  auto &m = get_or_calc_model_matrix();
  auto &v = view_matrix;
  auto &p = projection_matrix;
  auto &vp = view_projection_matrix;

  std::vector<rt::fl_t> vertices_buffer;
  std::vector<rt::fl_t> normals_buffer;
  std::vector<rt::idx_t> indices_buffer = geometry.get_indices();
  std::array<rt::fl_t, 16> model_matrix_buffer{};
  std::array<rt::fl_t, 16> view_matrix_buffer{};
  std::array<rt::fl_t, 16> projection_matrix_buffer{};
  std::array<rt::fl_t, 16> view_projection_matrix_buffer{};

  for (auto &vertex : vertices) {
    vertices_buffer.push_back(vertex.x);
    vertices_buffer.push_back(vertex.y);
    vertices_buffer.push_back(vertex.z);
  }

  for (auto &normal : normals) {
    normals_buffer.push_back(normal.x);
    normals_buffer.push_back(normal.y);
    normals_buffer.push_back(normal.z);
  }

  for (int i = 0; i < 4; ++i) {
    for (int j = 0; j < 4; ++j) {
      int index = i * 4 + j;
      model_matrix_buffer[index] = m[j][i];
      view_matrix_buffer[index] = v[j][i];
      projection_matrix_buffer[index] = p[j][i];
      view_projection_matrix_buffer[index] = vp[j][i];
    }
  }

#ifndef DEBUG
  printf("\n");
  for (int i = 0; i < 4; ++i) {
    printf("%f\t%f\t%f\t%f\n", m[0][i], m[1][i], m[2][i], m[3][i]);
  }

  printf("\n");
  for (int i = 0; i < 4; ++i) {
    printf("%f\t%f\t%f\t%f\n", v[0][i], v[1][i], v[2][i], v[3][i]);
  }

  printf("\n");
  for (int i = 0; i < 4; ++i) {
    printf("%f\t%f\t%f\t%f\n", p[0][i], p[1][i], p[2][i], p[3][i]);
  }

  printf("\n");
  for (int i = 0; i < 4; ++i) {
    printf("%f\t%f\t%f\t%f\n", vp[0][i], vp[1][i], vp[2][i], vp[3][i]);
  }
#endif

  indices_length = indices_buffer.size();

  glGenVertexArrays(1, &VAO);
  glBindVertexArray(VAO);
  glGenBuffers(VBO.size(), VBO.data());

  int i = -1;
  glBindBuffer(GL_ARRAY_BUFFER, VBO[++i]);
  glBufferData(GL_ARRAY_BUFFER, vertices_buffer.size() * sizeof(rt::fl_t), vertices_buffer.data(), GL_STATIC_DRAW);

  glBindBuffer(GL_ARRAY_BUFFER, VBO[++i]);
  glBufferData(GL_ARRAY_BUFFER, normals_buffer.size() * sizeof(rt::fl_t), normals_buffer.data(), GL_STATIC_DRAW);

  glBindBuffer(GL_ARRAY_BUFFER, VBO[++i]);
  glBufferData(GL_ARRAY_BUFFER, model_matrix_buffer.size() * sizeof(rt::fl_t), model_matrix_buffer.data(), GL_STATIC_DRAW);

  //  glBindBuffer(GL_ARRAY_BUFFER, VBO[++i]);
  //  glBufferData(GL_ARRAY_BUFFER, view_matrix_buffer.size() * sizeof(rt::fl_t), view_matrix_buffer.data(), GL_STATIC_DRAW);
  //
  //  glBindBuffer(GL_ARRAY_BUFFER, VBO[++i]);
  //  glBufferData(GL_ARRAY_BUFFER, projection_matrix_buffer.size() * sizeof(rt::fl_t), projection_matrix_buffer.data(), GL_STATIC_DRAW);

  glBindBuffer(GL_ARRAY_BUFFER, VBO[++i]);
  glBufferData(GL_ARRAY_BUFFER, view_projection_matrix_buffer.size() * sizeof(rt::fl_t), view_projection_matrix_buffer.data(),
               GL_STATIC_DRAW);

  glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, VBO[++i]);
  glBufferData(GL_ELEMENT_ARRAY_BUFFER, indices_buffer.size() * sizeof(rt::idx_t), indices_buffer.data(), GL_STATIC_DRAW);

  SPDLOG_LOGGER_INFO(rt::log::console, "Applied material to geometry. Vertices: {}, Indices: {}, Tris: {}. {}", vertices.size(),
                     indices_buffer.size(), indices_buffer.size() / 3,
                     indices_buffer.size() % 3 != 0 ? "Invalid configuration: count of indices must be multiple of 3" : "");
}

void rt::model::update_camera_matrices(const glm::mat4 &v, const glm::mat4 &p, const glm::mat4 &vp)
{
  view_matrix = v;
  projection_matrix = p;
  view_projection_matrix = vp;
}
