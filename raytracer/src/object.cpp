#include <raytracer/objects/object.hpp>

rt::object::object() = default;

rt::object::~object() = default;

const glm::mat4 &rt::object::get_or_calc_model_matrix()
{
  if (is_dirty) {
    glm::mat4 mat(1.0f);

    mat = glm::translate(mat, position) * rotation * glm::scale(mat, scale);
    model_matrix = mat;
    is_dirty = false;
  }
  return model_matrix;
}

const glm::vec3 &rt::object::get_position() const
{
  return position;
}

void rt::object::set_position(const glm::vec3 &position)
{
  object::position = position;
  is_dirty = true;
}

const glm::mat4 &rt::object::get_rotation() const
{
  return rotation;
}

void rt::object::set_rotation(const glm::mat4 &rotation_matrix)
{
  object::rotation = rotation_matrix;
  is_dirty = true;
}

const glm::vec3 &rt::object::get_scale() const
{
  return scale;
}

void rt::object::set_scale(const glm::vec3 &scale)
{
  object::scale = scale;
  is_dirty = true;
}

void rt::object::set_position(f32 x, f32 y, f32 z)
{
  position.x = x;
  position.y = y;
  position.z = z;
  is_dirty = true;
}

void rt::object::set_scale(f32 x, f32 y, f32 z)
{
  scale.x = x;
  scale.y = y;
  scale.z = z;
  is_dirty = true;
}
