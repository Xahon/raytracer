#include <raytracer/engine/raytracer.hpp>
#include <raytracer/engine/log.hpp>

rt::raytracer::raytracer() = default;

rt::raytracer::~raytracer()
{
  SDL_DestroyWindow(w);
  SDL_GL_DeleteContext(gl);
  w = nullptr;
  gl = nullptr;

  SDL_Quit();
}

void rt::raytracer::init()
{
  if (SDL_Init(SDL_INIT_EVERYTHING) < 0) {
    SPDLOG_LOGGER_ERROR(rt::log::console, "SDL init: %s", SDL_GetError());
    throw std::exception{};
  }

  SDL_GL_SetAttribute(SDL_GL_CONTEXT_PROFILE_MASK, SDL_GL_CONTEXT_PROFILE_CORE);
  SDL_GL_SetAttribute(SDL_GL_CONTEXT_MAJOR_VERSION, 3);
  SDL_GL_SetAttribute(SDL_GL_CONTEXT_MINOR_VERSION, 3);
  SDL_GL_SetAttribute(SDL_GL_STENCIL_SIZE, 8);

  w = SDL_CreateWindow(w_name.c_str(), 0, 0, w_width, w_height, SDL_WINDOW_OPENGL);
  if (w == nullptr) {
    SPDLOG_LOGGER_ERROR(rt::log::console, "SDL window: %s", SDL_GetError());
    throw std::exception{};
  }

  gl = SDL_GL_CreateContext(w);
  if (gl == nullptr) {
    SPDLOG_LOGGER_ERROR(rt::log::console, "SDL GL context: %s", SDL_GetError());
    throw std::exception{};
  }
  SPDLOG_LOGGER_INFO(rt::log::console, "GL context created. GL version {}", glGetString(GL_VERSION));

  glewExperimental = GL_TRUE;
  if (glewInit() != GLEW_OK) {
    SPDLOG_LOGGER_INFO(rt::log::console, "GLEW init error");
    throw std::exception{};
  }
  SPDLOG_LOGGER_INFO(rt::log::console, "GLEW initialized");
}

void rt::raytracer::handle_events()
{
  SDL_Event event;
  while (SDL_PollEvent(&event) != 0) {
    switch (event.type) {
      case SDL_QUIT:
      case SDL_WINDOWEVENT_CLOSE:
        is_running = false;
        break;
    }
  }
}

void rt::raytracer::run(void (*before_update_func)(const rt::dur_s &))
{
  if (!is_running && !is_stopped) {
    start_timer.start();
    last_frame_duration = rt::dur_s::zero();
    delta_duration = rt::dur_s::zero();
    frame_duration = rt::dur_s::zero();

    is_running = true;
    start();
    while (is_running) {
      handle_events();

      frame_duration = start_timer.get_time_from_start();
      delta_duration = frame_duration - last_frame_duration;

      const rt::dur_s &target_time = time_per_frame();
      if (delta_duration < target_time) {
        std::this_thread::sleep_for(target_time - delta_duration);
        delta_duration = target_time;
      }

      if (before_update_func != nullptr)
        before_update_func(delta_duration);
      update(delta_duration);
      render(delta_duration);

      last_frame_duration = start_timer.get_time_from_start();
      frame_duration = start_timer.get_time_from_start();
    }
    is_stopped = true;
  }
  else {
    SPDLOG_LOGGER_ERROR(rt::log::console, "Trying to run raytracer while it is already running or already stopped");
  }
}

rt::dur_s &rt::raytracer::time_per_frame()
{
  static rt::dur_s d;
  static int last_value = -1;
  if (last_value != max_fps) {
    d = rt::dur_s(1.0f / abs((f32)max_fps));
    last_value = max_fps;
  }
  return d;
}

void rt::raytracer::start()
{
  if (active_scene != nullptr)
    active_scene->start();
}

void rt::raytracer::update(const rt::dur_s &delta_time)
{
  if (active_scene != nullptr)
    active_scene->update(delta_time);
}

void rt::raytracer::render(const rt::dur_s &delta_time)
{
  if (active_scene != nullptr)
    active_scene->render(delta_time);
  SDL_GL_SwapWindow(w);
}

void rt::raytracer::set_active_scene(rt::scene *scene, rt::camera *camera)
{
  active_scene = scene;
  active_camera = camera;

  active_scene->set_camera(camera);
}
