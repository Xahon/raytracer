#include <raytracer/objects/scene.hpp>
#include <raytracer/engine/log.hpp>
#include <raytracer/objects/model.hpp>

rt::scene::scene() = default;

rt::scene::~scene() = default;

void rt::scene::add_object(rt::object *object)
{
  if (objects.find(object->get_id()) == objects.end()) {
    objects.insert({object->get_id(), object});
    SPDLOG_LOGGER_DEBUG(rt::log::console, "adding object with id {}. count of objects: {}", object->get_id(), objects.size());
  }
}

void rt::scene::remove_object(rt::unique_id *id)
{
  auto i = objects.find(id->get_id());
  bool found = i != objects.end();
  if (found) {
    objects.erase(i);
    SPDLOG_LOGGER_DEBUG(rt::log::console, "removing object with id {}. found: {}. count of objects: {}", id->get_id(), found,
                        objects.size());
  }
}

void rt::scene::start()
{
  for (auto &object : objects) {
    object.second->start();
  }
}

void rt::scene::update(const rt::dur_s &delta_time)
{
  if (active_camera != nullptr) {
    auto &v = active_camera->get_or_calc_view_matrix();
    auto &p = active_camera->get_or_calc_projection_matrix();
    auto vp = p * v;

    for (auto &object : objects) {
      auto *model = dynamic_cast<rt::model *>(object.second);
      if (model != nullptr) {
        model->update_camera_matrices(v, p, vp);
      }
      object.second->update(delta_time);
    }
  }
  else {
    SPDLOG_LOGGER_WARN(rt::log::console, "Scene doesn't have an active camera, so it won't render anything");
  }
}

void rt::scene::render(const rt::dur_s &delta_time)
{
  glClearColor(0, 0, 0, 1);
  glClear(GL_COLOR_BUFFER_BIT);

  for (auto &object : objects) {
    object.second->render(delta_time);
  }
}

void rt::scene::set_camera(rt::camera *camera)
{
  active_camera = camera;
}
