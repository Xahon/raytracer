#include <raytracer/utils/timer.hpp>

rt::timer::timer(int tick_interval) : tick_interval(tick_interval)
{
}

rt::timer::~timer()
{
  stop();
}

void rt::timer::create_thread_if_not_exists()
{
  if (thread_created) {
    return;
  }

  thread = std::thread([&]() {
    while (!this->is_stopped) {
      if (this->is_paused) {
        return;
      }
      auto sleep_time = rt::dur_s(this->tick_interval);
      std::this_thread::sleep_for(sleep_time);
      this->time_from_start += sleep_time;
      ++this->ticks;
      this->on_tick();
    }
  });
  thread_created = true;
}

void rt::timer::on_tick()
{
}

void rt::timer::start()
{
  if (!is_stopped) {
    create_thread_if_not_exists();
  }
}

void rt::timer::pause(bool pause_enabled)
{
  if (pause_enabled) {
    is_paused = false;
    create_thread_if_not_exists();
  }
  else {
    is_paused = true;
  }
}

void rt::timer::stop()
{
  is_stopped = true;
}

const rt::dur_s &rt::timer::get_time_from_start() const
{
  return time_from_start;
}

int rt::timer::get_ticks()
{
  return ticks;
}
