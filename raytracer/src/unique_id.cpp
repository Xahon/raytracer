#include <raytracer/objects/unique_id.hpp>

namespace {
size_t last_id = 0;
std::mutex id_mutex;
}  // namespace

rt::unique_id::unique_id()
{
  id_mutex.lock();
  id = ++last_id;
  id_mutex.unlock();
}

rt::unique_id::~unique_id() = default;

size_t rt::unique_id::get_id() const
{
  return id;
}