#include <gtest/gtest.h>
#include <raytracer/math/math.hpp>

#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Wunknown-pragmas"
#pragma ide diagnostic ignored "performance-unnecessary-copy-initialization"

using rt::matrix4;
using rt::vector3;

int main(int argc, char **argv)
{
  testing::InitGoogleTest(&argc, argv);
  return RUN_ALL_TESTS();
}

TEST(CML, Mat4_Ctors)
{
  matrix4<f32> m1({
  1, 0, 0, 1,  //
  0, 2, 1, 2,  //
  2, 1, 0, 1,  //
  2, 0, 1, 4,  //
  });

  matrix4<f32> m2(m1);
  ASSERT_EQ(m1, m2);

  matrix4<f32> m3(m2, rt::matrix4_init_style_op::RT_MAT_INIT_INVERTED);
  matrix4<f32> m3_expected({
  -2, -1.0f / 2, 1, 1.0f / 2,  //
  1, 1.0f / 2, 0, -1.0f / 2,   //
  -8, -1, 2, 2,                //
  3, 1.0 / 2, -1, -1.0 / 2,    //
  });
  ASSERT_EQ(m3, m3_expected);

  auto m4 = m2 * m3;
  matrix4<f32> m4_expected(1);

  ASSERT_EQ(m4, m4_expected);
}

TEST(CML, Mat4_Access)
{
  matrix4<f32> m({
  5, 6, 1, 2,  //
  3, 4, 7, 8,  //
  5, 1, 7, 6,  //
  8, 2, 2, 4,  //
  });

  ASSERT_EQ(m[0], 5);
  ASSERT_EQ(m[1], 6);
  ASSERT_EQ(m[2], 1);
  ASSERT_EQ(m[3], 2);

  ASSERT_EQ(m[4], 3);
  ASSERT_EQ(m[5], 4);
  ASSERT_EQ(m[6], 7);
  ASSERT_EQ(m[7], 8);

  ASSERT_EQ(m[8], 5);
  ASSERT_EQ(m[9], 1);
  ASSERT_EQ(m[10], 7);
  ASSERT_EQ(m[11], 6);

  ASSERT_EQ(m[12], 8);
  ASSERT_EQ(m[13], 2);
  ASSERT_EQ(m[14], 2);
  ASSERT_EQ(m[15], 4);

  ASSERT_EQ(m[0], m(0, 0));
  ASSERT_EQ(m[1], m(0, 1));
  ASSERT_EQ(m[2], m(0, 2));
  ASSERT_EQ(m[3], m(0, 3));

  ASSERT_EQ(m[4], m(1, 0));
  ASSERT_EQ(m[5], m(1, 1));
  ASSERT_EQ(m[6], m(1, 2));
  ASSERT_EQ(m[7], m(1, 3));

  ASSERT_EQ(m[8], m(2, 0));
  ASSERT_EQ(m[9], m(2, 1));
  ASSERT_EQ(m[10], m(2, 2));
  ASSERT_EQ(m[11], m(2, 3));

  ASSERT_EQ(m[12], m(3, 0));
  ASSERT_EQ(m[13], m(3, 1));
  ASSERT_EQ(m[14], m(3, 2));
  ASSERT_EQ(m[15], m(3, 3));
}

TEST(CML, Mat4_Mat4_Mul)
{
  using rt::matrix4;

  matrix4<f32> m1({
  5, 6, 1, 2,  //
  3, 4, 7, 8,  //
  5, 1, 7, 6,  //
  8, 2, 2, 4,  //
  });

  matrix4<f32> m2({
  6, 9, 7, 2,  //
  0, 1, 4, 4,  //
  1, 2, 9, 6,  //
  6, 5, 0, 1,  //
  });

  matrix4<f32> expected({
  43, 63, 68, 42,   //
  73, 85, 100, 72,  //
  73, 90, 102, 62,  //
  74, 98, 82, 40,   //
  });

  auto actual = m1 * m2;

  ASSERT_EQ(expected, actual);
}

TEST(CML, Mat4_Mat4_Add)
{
  matrix4<f32> m1({
  5, 6, 1, 2,  //
  3, 4, 7, 8,  //
  5, 1, 7, 6,  //
  8, 2, 2, 4,  //
  });

  matrix4<f32> m2({
  6, 9, 7, 2,  //
  0, 1, 4, 4,  //
  1, 2, 9, 6,  //
  6, 5, 0, 1,  //
  });

  matrix4<f32> expected({
  11, 15, 8, 4,  //
  3, 5, 11, 12,  //
  6, 3, 16, 12,  //
  14, 7, 2, 5,   //
  });

  auto actual = m1 + m2;

  ASSERT_EQ(expected, actual);
}

TEST(CML, Mat4_Mat4_Sub)
{
  matrix4<f32> m1({
  5, 6, 1, 2,  //
  3, 4, 7, 8,  //
  5, 1, 7, 6,  //
  8, 2, 2, 4,  //
  });

  matrix4<f32> m2({
  6, 9, 7, 2,  //
  0, 1, 4, 4,  //
  1, 2, 9, 6,  //
  6, 5, 0, 1,  //
  });

  matrix4<f32> expected({
  -1, -3, -6, 0,  //
  3, 3, 3, 4,     //
  4, -1, -2, 0,   //
  2, -3, 2, 3,    //
  });

  auto actual = m1 - m2;

  ASSERT_EQ(expected, actual);
}

TEST(CML, Vec3_Ctors)
{
  vector3<f32> v1(1, 2, 3);
  vector3<f32> v2(v1);
  ASSERT_EQ(v1, v2);

  vector3<f32> v3(v2, rt::vector3_init_style_op::RT_VEC_INIT_NORMALIZED);
  vector3<f32> v3_expected(0.26726, 0.5345, 0.8018);
  ASSERT_EQ(v3, v3_expected);

  auto v4 = v3 * v2.magnitude();
  ASSERT_EQ(v4, v1);
}

TEST(CML, Vec3_Access)
{
  vector3<f32> v(1, 2, 3);

  ASSERT_EQ(v[0], 1);
  ASSERT_EQ(v[1], 2);
  ASSERT_EQ(v[2], 3);
  ASSERT_EQ(v.x, 1);
  ASSERT_EQ(v.y, 2);
  ASSERT_EQ(v.z, 3);
  ASSERT_EQ(v.r, 1);
  ASSERT_EQ(v.g, 2);
  ASSERT_EQ(v.b, 3);
  v.x = 0;
  ASSERT_EQ(v[0], 0);
}

TEST(CML, Cross_Ops)
{
  vector3<f32> v(1, 2, 3);
  matrix4<f32> m(1);

  //  m.set_position(v);
}

#pragma clang diagnostic pop
