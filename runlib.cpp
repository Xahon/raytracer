#include <raytracer/engine/typedefs.hpp>
#include <raytracer/engine/raytracer.hpp>
#include <raytracer/objects/scene.hpp>
#include <raytracer/objects/model.hpp>
#include <raytracer/objects/camera.hpp>
#include <raytracer/engine/log.hpp>

namespace {
rt::raytracer *raytracer = nullptr;
rt::scene *scene = nullptr;
rt::camera *camera = nullptr;
rt::model *triangle = nullptr;
rt::geometry *triangleGeometry = nullptr;
rt::material *triangleMaterial = nullptr;
}

void createAndInitScene(rt::raytracer *raytracer) {
  scene = new(std::nothrow) rt::scene();
  camera = new(std::nothrow) rt::camera(static_cast<rt::f32>(raytracer->w_width) / static_cast<rt::f32>(raytracer->w_height));
  triangleGeometry = new(std::nothrow) rt::geometry();
  triangleMaterial = new(std::nothrow) rt::material(
    ""
    "#version 330 core\n"
    "layout (location = 0) in vec3 aPos;\n"
    "layout (location = 1) in vec3 aNrm;\n"
    "layout (location = 2) in mat4 m;\n"
    //    "layout (location = 3) in mat4 v;\n"
    //    "layout (location = 4) in mat4 p;\n"
    "layout (location = 3) in mat4 vp;\n"
    "out vec3 vNrm;\n"
    "out vec3 vPos;\n"
    "void main()\n"
    "{\n"
    "   vNrm = aNrm;\n"
    "   vPos = aPos;\n"
    "   gl_Position = vp * m * vec4(aPos.x, aPos.y, aPos.z, 1.0);\n"
    "}\0",
    ""
    "#version 330 core\n"
    "out vec4 FragColor;\n"
    "in vec3 vNrm;\n"
    "in vec3 vPos;\n"
    "void main()\n"
    "{\n"
    "   FragColor = vec4(1.0, 0.0, 0.0, 1.0);\n"
    "}\n\0",
    false
  );

  triangleGeometry->set_vertices(
    {
      glm::vec3(-1, 0, -1),
      glm::vec3(1, 0, -1),
      glm::vec3(0, 0, 1),
    }
  );
  triangleGeometry->set_normals(
    {
      glm::vec3(1, 0, 0),
      glm::vec3(1, 0, 0),
      glm::vec3(1, 0, 0),
    });
  triangleGeometry->set_indices(
    {0, 1, 2, 2, 1, 0}
  );

  triangle = new(std::nothrow) rt::model(*triangleGeometry, *triangleMaterial);

  camera->set_position(0, 1, 0);
  // glm quaternions aren't working
  // camera->set_rotation(glm::lookAt(camera->get_position(), glm::vec3(0, -1, 0), glm::vec3(0, 1, 0)));
  camera->recalculate_matrices();

  scene->add_object(camera);
  scene->add_object(triangle);

  raytracer->set_active_scene(scene, camera);
}

void before_update(const rt::dur_s &delta_time) {
//  triangle->set_position(triangle->get_position() + glm::vec3(0.01f, 0, 0) * delta_time.count());
//  SPDLOG_LOGGER_INFO(rt::log::console, "DT: {}s. Pos: {}, {}, {}", delta_time.count(), triangle->get_position().x, triangle->get_position().y,
//                     triangle->get_position().z);
}

int main() {
  raytracer = new(std::nothrow) rt::raytracer();
  if (raytracer == nullptr)
    return 1;

  raytracer->max_fps = 60;
  raytracer->init();
  createAndInitScene(raytracer);
  raytracer->run(before_update);

  delete raytracer;
  delete scene;
  delete camera;
  delete triangle;
  delete triangleGeometry;
  delete triangleMaterial;
  raytracer = nullptr;
  scene = nullptr;
  camera = nullptr;
  triangle = nullptr;
  triangleGeometry = nullptr;
  triangleMaterial = nullptr;

  return 0;
}